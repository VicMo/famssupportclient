<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSupportTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('support_tickets', function (Blueprint $table) {
            $table->id();
            $table->string('support_ticket_no');
            $table->string('contact_email');
            $table->string('company');
            $table->string('application');
            $table->string('priority');
            $table->string('support_category');      
            $table->longText('support_description');
            $table->string('created_by');
            $table->string('status')->default('0'); 
            $table->string('filename')->nullable();
            $table->string('mime')->nullable();
            $table->string('original_filename')->nullable();
            $table->string('solution_status')->nullable();
            $table->string('resolved_by')->nullable();
            $table->integer('man_hours')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('support_tickets');
    }
}
