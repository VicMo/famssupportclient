<?php

use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/', 'GuestTicketController@index')->name('welcome');
Route::post('/', 'GuestTicketController@store');


// Route::get('/create-ticket', function () {
//     return view('create-ticket.index');
// });

Auth::routes([
    'register' =>true
]);

Route::get('/home', 'HomeController@index')->name('home');

//create ticket
Route::get('/create-ticket', 'SupportTicketController@index');
Route::post('/create-ticket', 'SupportTicketController@store');


//Show the Dashboard;
Route::get('/dashboard', 'DashboardController@index')->name('dashboard');

//Show all tickets.abnf
Route::get('/alltickets','AllTicketsController@index')->name('alltickets'); //done
Route::get('/alltickets/{id}/ticket-details','AllTicketsController@show')->name('alltickets.ticket-details'); //done

//SHow all Cancelled tickets are
Route::get('/cancelledtickets','CancelledTicketsController@index')->name('cancelledtickets');
Route::get('/cancelledtickets/{id}/ticket-details','CancelledTicketsController@show')->name('cancelledtickets.ticket-details'); //done

//SHow all closed tickets
Route::get('/closedtickets','ClosedTicketsController@index')->name('closedtickets');
Route::get('/closedtickets/{id}/ticket-details','ClosedTicketsController@show')->name('closedtickets.ticket-details'); //done

//SHow all Open tickets
Route::get('/opentickets','OpenTicketsController@index')->name('opentickets');
Route::get('/opentickets/{id}/ticket-details','OpenTicketsController@show')->name('opentickets.ticket-details'); //done
// Route::post('/opentickets', 'SupportTicketController@edit')->name('opentickets');
Route::post('/opentickets/ticket-details','SupportTicketController@update')->name('cancel');
Route::post('/opentickets','SupportTicketController@cancel')->name('update');





//Route change password 
Route::get('change-password', 'ChangePasswordController@index');
Route::post('change-password', 'ChangePasswordController@store')->name('change.password');

//Manage Users
Route::get('manage-users', 'ManageUsersController@index');
Route::get('create-users', 'ManageUsersController@newuser');
Route::get('new-user', 'ManageUsersController@newuser');
Route::post('create-users', 'ManageUsersController@store');

//Manage Projects 
Route::get('projects', 'ProjectController@index');
Route::post('projects', 'ProjectController@store');






