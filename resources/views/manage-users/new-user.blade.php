@extends('layouts.master') @section('content') @can('isAdmin')
<main id="main-container">
    <div class="content">
        <h2 class="content-heading">Create User</h2>
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Create User</div>
                    <div class="card-body">

                        <form method="post" action="{{action('ManageUsersController@store')}}">
                            @csrf @foreach ($errors->all() as $error)
                            <p class="text-danger">{{ $error }}</p>
                            @endforeach
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="created_by" class="col-form-label">Name<span style="color:red;">*</span></label>
                                    <input type="text" class="form-control" id="created_by" required placeholder="Current Password" name="name">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="contact_email" class="col-form-label">Email<span style="color:red;">*</span></label>
                                    <input type="email" class="form-control" id="contact_email" required placeholder="New Password" name="email">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="user_type" class="col-form-label">User Type<span style="color:red;">*</span></label>
                                    <select id="application" class="form-control" required name="user_type" required>
                                        <option value="">Choose User Type</option>
                                        <option value="admin">Admin</option>
                                        <option value="certify">Certify</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <!-- <label for="contact_email" class="col-form-label">Password</label> -->
                                    <input type="password" hidden value="Tamarix@2020" class="form-control" id="password" placeholder="Confirm Password" name="password">
                                </div>
                            </div>

                            <button type="submit" class="btn btn-primary">Create Account</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@include('sweetalert::alert') @endcan @endsection