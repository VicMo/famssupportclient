@extends('layouts.master')

@section('content')
@can('isAdmin')
<main id="main-container">
    <div class="content">
        <h2 class="content-heading">Manage User</h2>
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Manage User</div>
                    <div class="card-body">
                        <table id="example1" class="table table-striped table-responsive">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Role</th>
                                    <th>Create date</th>
                                    <!-- <th>Creation Date</th>
                                    <th>Contact Email</th>
                                    <th>Support Title</th>
                                    <th>Support Description</th> -->

                                </tr>
                            </thead>
                            <tbody>
                                @forelse($view_users as $mytickets)
                                <tr>
                                    <td>{{ $mytickets->name }}</td>
                                    <td>{{ $mytickets->email }}</td>
                                    <td>{{ $mytickets->user_type }}</td>
                                    <td>{{ \Carbon\Carbon::parse($mytickets->created_at)->format('d/M/Y')}}</td>
                                    
                                </tr>
                                @empty
                                <tr>
                                    <p>No Data availabe</p>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@include('sweetalert::alert')
@endcan



@endsection