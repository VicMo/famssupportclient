<nav id="sidebar" class="sidenav">
    <div class="sidebar-wrapper">
        @auth
        <div class="profile-sidebar">
            <div class="avatar">
                <img src="{{asset('assets/images/profiles/05.jpg')}}" alt="">
            </div>
            <div class="profile-name">
                {{Auth::user()->name}}
                <!-- <button class="btn-prof" type="button" data-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-ellipsis-v"></i>
                </button> -->
                <!-- <div class="dropdown-menu">
                    <a class="dropdown-item" href="#"><span class="icon ti-user mr-3"></span>Profile</a>
                    <a class="dropdown-item" href="#"><span class="icon ti-email mr-3"></span>Inbox</a>
                    <a class="dropdown-item" href="#"><span class="icon ti-settings mr-3"></span>Settings</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#"><span class="icon ti-power-off mr-3"></span>Logout</a>
                </div> -->
            </div>
            <div class="profile-title">
                {{Auth::user()->email}}</div>
        </div>
        @else
        <div class="profile-sidebar">
            <div class="avatar">
                <img src="{{asset('assets/images/profiles/05.jpg')}}" alt="">
            </div>
            <div class="profile-name">
                Guest
                <!-- <button class="btn-prof" type="button" data-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-ellipsis-v"></i>
                </button> -->
                <!-- <div class="dropdown-menu">
                    <a class="dropdown-item" href="#"><span class="icon ti-user mr-3"></span>Profile</a>
                    <a class="dropdown-item" href="#"><span class="icon ti-email mr-3"></span>Inbox</a>
                    <a class="dropdown-item" href="#"><span class="icon ti-settings mr-3"></span>Settings</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#"><span class="icon ti-power-off mr-3"></span>Logout</a>
                </div> -->
            </div>
            <div class="profile-title">
                Guest Account</div>
        </div>
        @endauth

        <ul class="main-menu" id="menus">
            @auth
            <li>
                <a href="{{url('/home')}}">
                    <span class="icon ti-home"></span>Home
                </a>
            </li>
            <li class="header">Support Ticket</li>

            <li>
                <a href="{{url('create-ticket')}}">
                    <span class="icon ti-home"></span>Create Support Ticket
                </a>
            </li>

            <li>
                <a class="pr-mn collapsed" data-toggle="collapse" href="#uielements" aria-expanded="true">
                    <span class="icon ti-notepad"></span>Support Tickets
                </a>
                <ul id="uielements" class="collapse" data-parent="#menus">
                    <li><a href="{{url('alltickets')}}">All Tickets</a></li>
                    <li><a href="{{url('opentickets')}}">Open Tickets</a></li>
                    <li><a href="{{url('closedtickets')}}">Closed Tickets</a></li>
                    <li><a href="{{url('cancelledtickets')}}">Cancelled Tickets</a></li>
                </ul>
            </li>
            <li class="header">Profile</li>
            <li>
                <a class="pr-mn collapsed" data-toggle="collapse" href="#genericpage" aria-expanded="true">
                    <span class="icon ti-user"></span>Account
                </a>
                <ul id="genericpage" class="collapse" data-parent="#menus">
                    <li><a href="{{url('change-password')}}">Change Password</a></li>
                </ul>
            </li>
            @can('isAdmin')
            <li class="header">Administration</li>
            <li>
                <a class="pr-mn collapsed" data-toggle="collapse" href="#administration" aria-expanded="true">
                    <span class="icon ti-user"></span>Manage Users
                </a>
                <ul id="administration" class="collapse" data-parent="#menus">
                    <li><a href="{{url('manage-users')}}">Manage Users</a></li>
                    <li><a href="{{url('create-users')}}">Create Users</a></li>

                </ul>
            </li>
            <li>
                <a href="{{url('projects')}}">
                    <span class="icon ti-home"></span>Add Client
                </a>
            </li>

            @endcan
            <!-- <li class="header">Miscellaneous</li>
            <li>
                <a class="pr-mn collapsed" data-toggle="collapse" href="#chart" aria-expanded="true">
                    <span class="icon ti-bar-chart"></span>Charts
                </a>
                <ul id="chart" class="collapse" data-parent="#menus">
                    <li><a href="chart-js.html">Chart.js</a></li>
                    <li><a href="chart-echarts.html">Echart</a></li>
                    <li><a href="chart-flot.html">Flot Chart</a></li>
                    <li><a href="chart-high.html">High Chart</a></li>
                    <li><a href="chart-morris.html">Morris Chart</a></li>
                </ul>
            </li>
            <li>
                <a class="pr-mn collapsed" data-toggle="collapse" href="#maps" aria-expanded="true">
                    <span class="icon ti-location-pin"></span>Maps
                </a>
                <ul id="maps" class="collapse" data-parent="#menus">
                    <li><a href="maps-google.html">Google Map</a></li>
                    <li><a href="maps-vector.html">Vector Map</a></li>
                </ul>
            </li>
            <li>
                <a href="helpers.html">
                    <span class="icon ti-help"></span>Helpers
                </a>
            </li> -->
            @else
            <!-- <li class="header">Support Ticket</li>
            <li>
                <a href="{{url('create-ticket')}}">
                    <span class="icon ti-home"></span>Create Support Ticket
                </a>
            </li> -->

            <li class="header">Account</li>
            <li>
                <a href="{{route('login')}}">
                    <span class="icon ti-home"></span>Login
                </a>
            </li>
            @endauth


            @guest
            @else
            <li>
                <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                    <span class="icon ti-help"></span>{{ __('Logout') }}
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </li>

            @endguest

        </ul>
    </div>
</nav>