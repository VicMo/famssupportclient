<form method="post" action="{{action('SupportTicketController@store')}}" enctype='multipart/form-data'>
    @csrf
    <div class="form-row">
        <div class="form-group col-md-4">
            <label for="Support Ticket Number" class="col-form-label">Support Ticket Number</label>
            <input type="text" readonly value="<?php echo date("Ymdhms"); ?>" class="form-control" id="support_ticket_no" name="support_ticket_no" placeholder="support_ticket_no">
        </div>
        <div class="form-group col-md-4">
            <label for="created_by" class="col-form-label">Ticket Created by</label>
            <input type="text" class="form-control" id="created_by" placeholder="Ticket Created by" name="created_by">
        </div>
        <div class="form-group col-md-4">
            <label for="contact_email" class="col-form-label">Contact Email</label>
            <input type="email" class="form-control" id="contact_email" placeholder="Contact Email" name="contact_email">
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-4">
            <label for="company" class="col-form-label">Select Company</label>
            <select id="company" class="form-control" name="company" required>
                <option>Choose Company</option>
                <option value="britam">Britam</option>
                <option value="cic">CIC</option>
            </select>
        </div>
        <div class="form-group col-md-4">
            <label for="application" class="col-form-label">Select Application/Project</label>
            <select id="application" class="form-control" name="application" required>
                <option>Choose Application/Project</option>
                <option value="app1">APP1</option>
                <option value="fams">FAMS</option>
            </select>
        </div>
        <div class="form-group col-md-4">
            <label for="priority" class="col-form-label">Service Priority</label>
            <select id="priority" class="form-control" name="priority" required>
                <option value="">Choose Priority</option>
                <option value="1">High (System Completely Unavailable | Response: < 2 Hours)</option> <option value="2">Medium (Critical System Functionality Unavailable | Response: 2-6 Hours)</option>
                <option value="3">Low (General System Issue | Response: < 24 Hours)</option> </select> </div> </div> <div class="form-row">
                        <div class="form-group col-md-8">
                            <label for="Support Request Title" class="col-form-label">Support Request Title</label>
                            <input type="text" class="form-control" id="support Request Title" placeholder="Support Request Title" name="support_category">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="inputPassword4" class="col-form-label">Attachement</label>
                            <input type="file" class="form-control" id="attachement" placeholder="Attach File (Files max 5Mb)" name="attachement">
                        </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-12">
                <label for="inputAddress2" class="col-form-label">Support Request Description: Max Characters: 1000</label>
                <textarea class="form-control" name="support_description" rows=11 cols=50 maxlength=1000 required></textarea>
            </div>
            <div class="form-group">
                <input type="text" value="1" readonly class="form-control" hidden id="status" name="status">
            </div>
        </div>

        <button type="submit" class="btn btn-primary">Submit Ticket</button>
</form>