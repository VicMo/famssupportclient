@extends('layouts.master') 
@section('content')
@can('isAdmin')
<main id="main-container">
    <div class="content">
        <h2 class="content-heading">Create Project</h2>
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Create Project</div>
                    <div class="card-body">

                        <form method="post" action="{{action('ProjectController@store')}}">
                            @csrf @foreach ($errors->all() as $error)
                            <p class="text-danger">{{ $error }}</p>
                            @endforeach
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="created_by" class="col-form-label">Company<span style="color:red;">*</span></label>
                                    <input type="text" class="form-control" id="created_by" required placeholder="Company" name="company">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="contact_email" class="col-form-label">Project Name<span style="color:red;">*</span></label>
                                    <input type="text" class="form-control" id="contact_email" required placeholder="Project" name="project">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="created_by" class="col-form-label">Start Date<span style="color:red;">*</span></label>
                                    <input type="date" class="form-control" id="created_by" required placeholder="start date" name="start_date">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="created_by" class="col-form-label">End Date Date (Estimate: Optional)</label>
                                    <input type="date" class="form-control" id="created_by" placeholder="end date" name="end_date">
                                </div>
                            </div>
                            <!-- <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="created_by" class="col-form-label">Created by<span style="color:red;">*</span></label> -->
                                    <input type="text" hidden readonly class="form-control" id="created_by" value="{{ Auth::user()->email}}" required placeholder="Created by" name="created_by">
                                <!-- </div>
                            </div> -->
                            

                            <button type="submit" class="btn btn-primary">Create Project</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>



        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">Cancelled tickets</div>
                    <div class="card-body">
                        <table id="example1" class="table table-striped table-responsive">
                            <thead>
                                <tr>
                                    <!-- <th></th> -->
                                    <th>Company</th>
                                    <th>Project</th>
                                    <th>Start date</th>
                                    <th>End date</th>
                                    <th>Creation Date</th>
                                    <!-- <th>Contact Email</th> -->
                                    <!-- <th>Support Title</th> -->
                                    <!-- <th>Support Description</th>                                     -->
                                </tr>
                            </thead>                            
                            <tbody>
                                @foreach($projects as $project)
                                <tr>
                                <!-- <td><a href="{{ route('cancelledtickets.ticket-details',['id'=>$project->id])}}">{{ $project->support_ticket_no }}</a></td> -->
                                    <td>{{ $project->company }}</td>
                                    <td>{{ $project->project }}</td>
                                    <td>{{ $project->start_date }}</td>
                                    <td>{{ $project->end_date}}</td>
                                    <td>{{ \Carbon\Carbon::parse($project->created_at)->format('d/M/Y')}}</td>
                                    <!-- <td>{{ $project->contact_email }}</td> -->
                                </tr>                                
                                @endforeach                                
                            </tbody>
                            
                        </table>
                    </div>
                </div>
            </div>
        </div>


    </div>
</main>
@endcan
@include('sweetalert::alert') 
@endsection