@extends('layouts.master')

@section('content')
<main id="main-container">
    <div class="content">
        <h2 class="content-heading">Chnage Password</h2>
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Change Password</div>
                    <div class="card-body">
                        <form method="post" action="{{action('ChangePasswordController@store')}}">
                            @csrf
                            @foreach ($errors->all() as $error)
                            <p class="text-danger">{{ $error }}</p>
                            @endforeach
                            <div class="form-row">
                                <div class="form-group col-md-3">
                                    <label for="created_by" class="col-form-label">Current Password</label>
                                    <input type="password" class="form-control" id="created_by" placeholder="Current Password" name="current_password">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="contact_email" class="col-form-label">New Password</label>
                                    <input type="password" class="form-control" id="contact_email" placeholder="New Password" name="new_password">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="contact_email" class="col-form-label">Confirm Password</label>
                                    <input type="password" class="form-control" id="contact_email" placeholder="Confirm Password" name="new_confirm_password">
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">Change Password</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@include('sweetalert::alert')

@endsection