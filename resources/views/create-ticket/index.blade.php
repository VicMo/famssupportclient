@extends('layouts.master') @section('content')
<main id="main-container">
    <div class="content">
        <h2 class="content-heading">Create Support Ticket</h2>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Create a Support Request</div>
                    <div class="card-body">
                        <form method="post" action="{{action('SupportTicketController@store')}}" enctype='multipart/form-data'>
                            @csrf
                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label for="Support Ticket Number" class="col-form-label">Support Ticket Number<span style="color:red;">*</span></label>
                                    <input type="text" required readonly value="<?php echo date(" Ymdhms "); ?>" class="form-control" id="support_ticket_no" name="support_ticket_no" placeholder="support_ticket_no">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="created_by" class="col-form-label">Ticket Created by<span style="color:red;">*</span></label>
                                    <input type="text" class="form-control" id="created_by" readonly value="{{Auth::user()->name}}" required placeholder="Ticket Created by" name="created_by">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="contact_email" class="col-form-label">Contact Email<span style="color:red;">*</span></label>
                                    <input type="email" required class="form-control" id="contact_email" readonly value="{{Auth::user()->email}}" placeholder="Contact Email" name="contact_email">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label for="company" class="col-form-label">Select Company<span style="color:red;">*</span></label>
                                    <select id="company" class="form-control" name="company">
                                        <option value="">Choose Company</option>
                                        @foreach($projects as $project)
                                        <option value="{{$project->company}}">{{$project->company}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="application" class="col-form-label">Select Application/Project<span style="color:red;">*</span></label>
                                    <select id="application" class="form-control" name="application">
                                        <option value="">Choose Application/Project</option>
                                        @foreach($projects as $project)
                                        <option value="{{$project->project}}">{{$project->project}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="priority" class="col-form-label">Priority<span style="color:red;">*</span></label>
                                    <select id="priority" class="form-control" name="priority">
                                        <option value="">Choose Priority</option>
                                        <option value="high">High (System Completely Unavailable | Response:< 2 Hours)</option>
                                        <option value="medium">Medium (Critical System Functionality Unavailable | Response: 2-6 Hours)</option>
                                        <option value="low">Low (General System Issue | Response:< 24 Hours)</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-8">
                                    <label for="Support Request Title" class="col-form-label">Support Request Title<span style="color:red;">*</span></label>
                                    <input type="text" class="form-control" id="support Request Title" placeholder="Support Request Title" name="support_category">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="inputPassword4" class="col-form-label">Attachement</label>
                                    <input type="file" class="form-control" id="attachement" placeholder="Attach File (Files max 5Mb)" name="attachement">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label for="inputAddress2" class="col-form-label">Support Request Description: Max Characters: 1000<span style="color:red;">*</span></label>
                                    <textarea class="form-control" name="support_description" rows=11 cols=50 maxlength=1000 required></textarea>
                                </div>
                                <div class="form-group">
                                    <input type="text" value="1" readonly class="form-control" hidden id="status" name="status">
                                </div>
                            </div>

                            <button type="submit" class="btn btn-primary">Submit Ticket</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@include('sweetalert::alert') @endsection