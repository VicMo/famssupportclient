@component('mail::message')

# {{ $details['title'] }}  

The body of your message.    

@component('mail::button', ['url' => $details['url']])

Login to Account

@endcomponent   

Thanks,<br>
{{ config('app.name') }}
@endcomponent
