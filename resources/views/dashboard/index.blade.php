<!DOCTYPE html>
<html lang="en-US">
<head>
    @include('shared.head')
</head>
<body>
    <div id="page-container">
        @include('shared.nav')
        @include('shared.header')
        <aside id="right-sidebar" class="r_sidebar">
            <div class="content-wrapper"><a href="javascript:void(0)" class="close-btn">
                    <span class="ti-close"></span>
                </a>
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="today-tab" data-toggle="tab" href="#today" aria-expanded="true">Today</a></li>
                    <li class="nav-item"><a class="nav-link" id="setting-tab" data-toggle="tab" href="#setting">Setting</a>
                    </li>
                </ul>
                <div class="tab-content sidebar-wrapper scrollbar-inner" id="myTabContent">
                    <div class="tab-pane fade show active" id="today">
                        <div class="today-date"><span class="strong"><span id="prMonth"></span> <span id="prDate"></span></span>, <span id="prYear"></span>
                            <span id="prDay"></span>
                        </div>
                        <div class="block-tab">
                            <div class="block-title"><span class="ti-time"></span> Schedule
                            </div>
                            <ul class="schedule-list list-unstyled">
                                <li>
                                    <div class="time">
                                        09.00<span>AM</span></div>
                                    <div class="point"></div>
                                    <div class="schedule-info">
                                        Briefing with product division
                                        <span class="location"><span class="ti-location-pin"></span> New York, NA</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="time">11.00<span>AM</span>
                                    </div>
                                    <div class="point"></div>
                                    <div class="schedule-info">
                                        Meeting with client
                                        <span class="location"><span class="ti-location-pin"></span> Client office</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="time">
                                        01.30<span>PM</span></div>
                                    <div class="point"></div>
                                    <div class="schedule-info">
                                        Project commisioning<span class="location"><span class="ti-location-pin"></span> Office</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="time">
                                        04.00<span>AM</span></div>
                                    <div class="point"></div>
                                    <div class="schedule-info">
                                        Public discussion at office<span class="location"><span class="ti-location-pin"></span> Cafetaria</span>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="block-tab">
                            <div class="block-title"><span class="ti-flag"></span> Activity Log
                            </div>
                            <ul class="activity-list list-unstyled">
                                <li>
                                    <div class="icon">
                                        <span class="ti-image"></span>
                                    </div>
                                    <div class="log-info">
                                        Photo profile has been updated<small>2 min ago</small>
                                    </div>
                                </li>
                                <li>
                                    <div class="icon"><span class="ti-email"></span>
                                    </div>
                                    <div class="log-info">
                                        New email to <strong>John Cenna</strong> sent<small>4 hrs ago</small>
                                    </div>
                                </li>
                                <li>
                                    <div class="icon"><span class="ti-email"></span>
                                    </div>
                                    <div class="log-info">You compose new email
                                        <small>6 hrs ago</small></div>
                                </li>
                                <li>
                                    <div class="icon">
                                        <span class="ti-email"></span></div>
                                    <div class="log-info">You compose new email
                                        <small>1 day ago</small></div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="setting">
                        <ul class="setting-list list-unstyled">
                            <li class="header">
                                Main system</li>
                            <li>
                                <div class="setting-name">
                                    Notifications</div>
                                <div class="switch"><input type="checkbox" class="js-switch" data-size="small" checked />
                                </div>
                            </li>
                            <li>
                                <div class="setting-name">
                                    Auto updates
                                </div>
                                <div class="switch">
                                    <input type="checkbox" class="js-switch" />
                                </div>
                            </li>
                            <li>
                                <div class="setting-name">
                                    Location
                                </div>
                                <div class="switch">
                                    <input type="checkbox" class="js-switch" data-size="small" checked />
                                </div>
                            </li>
                            <li class="header">Assistant
                            </li>
                            <li>
                                <div class="setting-name">
                                    Show Assistant</div>
                                <div class="switch">
                                    <input type="checkbox" class="js-switch" data-size="small" />
                                </div>
                            </li>
                            <li class="header">
                                Appearances
                            </li>
                            <li>
                                <div class="setting-name">
                                    Save history
                                </div>
                                <div class="switch">
                                    <input type="checkbox" class="js-switch" data-size="small" checked />
                                </div>
                            </li>
                            <li>
                                <div class="setting-name">
                                    Quick results
                                </div>
                                <div class="switch">
                                    <input type="checkbox" class="js-switch" data-size="small" />
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </aside>


        <!-- MAIN CONTAINER -->
        <main id="main-container">
            <div class="content">

                <div class="row">
                    <div class="col-lg-3 col-md-6 col-sm-6">

                        <!-- Item sold -->
                        <div class="card stats-card">
                            <div class="stats-icon">
                                <span class="ti-bag"></span>
                            </div>
                            <div class="stats-ctn">
                                <div class="stats-counter"><span class="counter">87</span></div>
                                <span class="desc">item sold</span>
                            </div>
                        </div><!-- .card -->
                        <!-- /End Item sold -->

                    </div><!-- .col -->
                    <div class="col-lg-3 col-md-6 col-sm-6">

                        <!-- Earnings -->
                        <div class="card stats-card">
                            <div class="stats-icon">
                                <span class="ti-wallet"></span>
                            </div>
                            <div class="stats-ctn">
                                <div class="stats-counter">$<span class="counter">1021</span></div>
                                <span class="desc">earnings</span>
                            </div>
                        </div><!-- .card -->
                        <!-- /End Earnings -->

                    </div><!-- .col -->
                    <div class="col-lg-3 col-md-6 col-sm-6">

                        <!-- Messages -->
                        <div class="card stats-card">
                            <div class="stats-icon">
                                <span class="ti-email"></span>
                            </div>
                            <div class="stats-ctn">
                                <div class="stats-counter"><span class="counter">31</span></div>
                                <span class="desc">messages</span>
                            </div>
                        </div><!-- .card -->
                        <!-- /End Messages -->

                    </div><!-- .col -->
                    <div class="col-lg-3 col-md-6 col-sm-6">

                        <!-- Notifications -->
                        <div class="card stats-card">
                            <div class="stats-icon">
                                <span class="ti-bell"></span>
                            </div>
                            <div class="stats-ctn">
                                <div class="stats-counter"><span class="counter">22</span></div>
                                <span class="desc">notifications</span>
                            </div>
                        </div><!-- .card -->
                        <!-- /End Notifications -->

                    </div><!-- .col -->
                </div><!-- .row -->

                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-7 col-xl-9 dev-resource-card">

                        <!-- Device resource -->
                        <div class="card">
                            <div class="card-body">
                                <div class="card-options">
                                    <button type="button" class="btn-option" id="refresh1">
                                        <i class="fa fa-refresh"></i>
                                    </button>
                                    <button type="button" class="btn-option" data-toggle="dropdown">
                                        <i class="fa fa-ellipsis-v"></i>
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item" href="#">Save</a>
                                        <a class="dropdown-item" href="#">Report</a>
                                        <a class="dropdown-item" href="#">Hide</a>
                                    </div>
                                </div>
                                <div class="card-header-inside">
                                    Device Resource
                                </div>

                                <div class="refresh-container">
                                    <i class="refresh-spinner fa fa-circle-o-notch fa-spin fa-5x"></i>
                                </div>

                                <canvas id="chart_verticalBar" style="width: 100%;"></canvas>
                            </div><!-- .card-body -->
                        </div><!-- .card -->
                        <!-- /End Device resource -->

                    </div><!-- .col -->
                    <div class="col-12 col-sm-12 col-md-12 col-lg-5 col-xl-3 server-load">

                        <!-- Server load -->
                        <div class="card">
                            <div class="card-body">
                                <div class="card-header-inside">
                                    Server Load
                                </div>
                                <div class="pie-chart-container">
                                    <canvas id="chart_pie" style="width: 100%;"></canvas>
                                </div>

                                <table class="table table-server mt-3">
                                    <thead>
                                        <tr>
                                            <th>Server</th>
                                            <th>Users</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td style="width:45%">Mail</td>
                                            <td>104,962 <span class="badge badge-success">normal</span></td>
                                        </tr>
                                        <tr>
                                            <td>DNS</td>
                                            <td>19,023 <span class="badge badge-success">normal</span></td>
                                        </tr>
                                        <tr>
                                            <td>Router</td>
                                            <td>11,107 <span class="badge badge-info">absolete</span></td>
                                        </tr>
                                        <tr>
                                            <td>Blog</td>
                                            <td>76,551 <span class="badge badge-warning">critical</span></td>
                                        </tr>
                                    </tbody>
                                </table>

                            </div><!-- .card-body -->
                        </div><!-- .card -->
                        <!-- /End Server load -->

                    </div><!-- .col -->
                </div><!-- .row -->

                <div class="row">
                    <div class="col-12">

                        <!-- Invoice -->
                        <div class="card">
                            <div class="card-body">
                                <div class="card-header-inside">
                                    Invoice
                                </div>

                                <table id="mytable" class="table table-striped table-responsive">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Company</th>
                                            <th>Payment Type</th>
                                            <th>Date</th>
                                            <th>Progress</th>
                                            <th>Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>#71990</td>
                                            <td>Carasco Petroloum, Inc</td>
                                            <td>PayPal</td>
                                            <td>Nov 11, 13:42</td>
                                            <td><span class="pie">2/8</span></td>
                                            <td>$689,999</td>
                                        </tr>
                                        <tr>
                                            <td>#71989</td>
                                            <td>Orange Association</td>
                                            <td>PayPal</td>
                                            <td>Nov 10, 09:00</td>
                                            <td><span class="pie">5/8</span></td>
                                            <td>$320,800</td>
                                        </tr>
                                        <tr>
                                            <td>#71988</td>
                                            <td>Johnson Law & Associates</td>
                                            <td>Wire Transfer</td>
                                            <td>Nov 09, 16:10</td>
                                            <td><span class="pie">6/8</span></td>
                                            <td>$920,720</td>
                                        </tr>
                                        <tr>
                                            <td>#71987</td>
                                            <td>Zahra Pte Ltd</td>
                                            <td>PayPal</td>
                                            <td>Nov 09, 14:22</td>
                                            <td><span class="pie">7/8</span></td>
                                            <td>$632,000</td>
                                        </tr>
                                        <tr>
                                            <td>#71986</td>
                                            <td>Kirba Brothers & Co.</td>
                                            <td>Transfer</td>
                                            <td>Nov 09, 10:12</td>
                                            <td><span class="pie">7/8</span></td>
                                            <td>$555,025</td>
                                        </tr>
                                        <tr>
                                            <td>#71985</td>
                                            <td>SkyNet Construction</td>
                                            <td>PayPal</td>
                                            <td>Nov 08, 12:05</td>
                                            <td><span class="pie">5/8</span></td>
                                            <td>$1120,360</td>
                                        </tr>
                                        <tr>
                                            <td>#71984</td>
                                            <td>Damon Industries</td>
                                            <td>Wire Transfer</td>
                                            <td>Nov 08, 08:25</td>
                                            <td><span class="pie">6/8</span></td>
                                            <td>$677,741</td>
                                        </tr>
                                        <tr>
                                            <td>#71983</td>
                                            <td>LexCorp</td>
                                            <td>PayPal</td>
                                            <td>Nov 07, 09:00</td>
                                            <td><span class="pie">8/8</span></td>
                                            <td>$190,000</td>
                                        </tr>
                                        <tr>
                                            <td>#71982</td>
                                            <td>AsCorp Industries</td>
                                            <td>Wire Transfer</td>
                                            <td>Nov 06, 15:30</td>
                                            <td><span class="pie">7/8</span></td>
                                            <td>$418,100</td>
                                        </tr>
                                        <tr>
                                            <td>#71981</td>
                                            <td>Ox Brothers & Co.</td>
                                            <td>Wire Transfer</td>
                                            <td>Nov 06, 09:30</td>
                                            <td><span class="pie">5/8</span></td>
                                            <td>$988,000</td>
                                        </tr>
                                        <tr>
                                            <td>#71980</td>
                                            <td>Alex Pte Ltd</td>
                                            <td>PayPal</td>
                                            <td>Nov 05, 17:10</td>
                                            <td><span class="pie">8/8</span></td>
                                            <td>$500,800</td>
                                        </tr>

                                    </tbody>
                                </table>

                            </div><!-- .card-body -->
                        </div><!-- .card -->
                        <!-- /End Invoice -->

                    </div><!-- .col -->
                </div><!-- .row -->

            </div><!-- .content -->
        </main>
        <footer id="page-footer" class="pagefooter">
            <div class="content">
                <div class="row">
                    <div class="copyright col-sm-12 col-md-12 col-lg-6">
                        &copy; 2017 Prudence - Bootstrap Admin Template</div>
                    <div class="footer-nav col-sm-12 col-md-12 col-lg-6">
                        <a href="javascript:void(0)">About</a>
                        <a href="javascript:void(0)">Terms</a></div>
                </div>
            </div>
        </footer>
    </div>

    @include('shared.scripts')
</body>

<!-- Mirrored from html.alfisahr.com/prudence/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 23 Feb 2019 00:47:48 GMT -->

</html>