@extends('layouts.master')
@section('content')
<main id="main-container">
    <div class="content">
        <h2 class="content-heading">Ticket Number: {{$supportticket->support_ticket_no}} </h2>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3">
                <div class="card">
                    <div class="card-header">Attachement</div>
                    <div class="card-body">
                        <p>Name: {{$supportticket->original_filename}} </p>
                        <button type="button" class="btn btn-block btn-primary" data-toggle="modal" data-target="#exampleModalFive">
                            Download Attachement
                        </button>
                        <hr />
                        @can('isUser')
                        <a href="#"><button type="button" class="btn btn-block btn-primary" data-toggle="modal" data-target="#exampleModalFive">
                                Respond to Ticket
                            </button></a>
                        @endcan

                    </div><!-- .card-body -->
                </div>

            </div><!-- .col -->

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-9">

                <!-- Options -->
                <div class="card">
                    <div class="card-header">Details</div>
                    <div class="card-body">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Company</th>
                                    <th>Application</th>
                                    <th>Priority</th>
                                    <th>Date Created</th>
                                    <!-- <th>Description</th>
                                    <th>Status</th> -->
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>{{$supportticket->company}}</td>
                                    <td>{{$supportticket->application}}</td>
                                    <td>{{$supportticket->priority}}</td>
                                    <td>{{ \Carbon\Carbon::parse($supportticket->created_at)->format('d/M/Y')}}</td>
                                    <!-- <td>The amount of time to delay between automatically cycling an item. If
                                        false, carousel will not automatically cycle.</td> -->
                                </tr>
                            </tbody>
                        </table>
                        <hr />

                        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-expanded="true">Title</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-expanded="true">Description</a>
                            </li>
                            <!-- <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Dropdown</a>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" id="pills-dropdown1-tab" href="#pills-dropdown1" role="tab" data-toggle="pill" aria-controls="pills-dropdown1">@fat</a>
                                    <a class="dropdown-item" id="pills-dropdown2-tab" href="#pills-dropdown2" role="tab" data-toggle="pill" aria-controls="pills-dropdown2">@mdo</a>
                                </div>
                            </li> -->
                        </ul>
                        <div class="tab-content" id="pills-tabContent">
                            <div class="tab-pane fade show active" id="pills-home" role="tabpanel">
                                {{$supportticket->support_category}}
                            </div>
                            <div class="tab-pane fade" id="pills-profile" role="tabpanel">
                                {{$supportticket->support_description}}
                            </div>
                            <!-- <div class="tab-pane fade" id="pills-dropdown1" role="tabpanel">
                                Culpa dolor voluptate do laboris laboris irure reprehenderit id incididunt duis
                                pariatur mollit aute magna pariatur consectetur. Eu veniam duis non ut dolor
                                deserunt commodo et minim in quis laboris ipsum velit id veniam. Quis ut
                                consectetur adipisicing officia excepteur non sit. Ut et elit aliquip labore
                                Lorem enim eu. Ullamco mollit occaecat dolore ipsum id officia mollit qui esse
                                anim eiusmod do sint minim consectetur qui.
                            </div>
                            <div class="tab-pane fade" id="pills-dropdown2" role="tabpanel">
                                Eu dolore ea ullamco dolore Lorem id cupidatat excepteur reprehenderit
                                consectetur elit id dolor proident in cupidatat officia. Voluptate excepteur
                                commodo labore nisi cillum duis aliqua do. Aliqua amet qui mollit consectetur
                                nulla mollit velit aliqua veniam nisi id do Lorem deserunt amet. Culpa ullamco
                                sit adipisicing labore officia magna elit nisi in aute tempor commodo eiusmod.
                            </div> -->
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</main>
@stop