@extends('layouts.master')
@section('content')
<main id="main-container">
    <div class="content">
        <h2 class="content-heading">Ticket Number: {{$supportticket->support_ticket_no}} </h2>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3">
                <div class="card">
                    <div class="card-header">Attachment</div>
                    <div class="card-body">
                        @if($supportticket->original_filename !== null)
                        <!-- <p>Attachment: {{$supportticket->original_filename}} </p> -->
                        <a href="{{url('uploads/'.$supportticket->filename)}}" target="_blank">
                            <button type="button" class="btn btn-block btn-primary" data-toggle="modal" data-target="#exampleModalFive">
                                View Attachment
                            </button>
                        </a>
                        @else
                        <div class="card-header">No Attachment Available</div>
                        @endif


                        @can('isCertify')
                        <hr />
                        <a href="#respond"><button type="button" class="btn btn-block btn-primary" data-toggle="modal" data-target="#exampleModalFive">
                                Respond to Ticket
                            </button></a>
                        @endcan

                        @can('isAdmin')
                        <hr />
                        <a href="#respond"><button type="button" class="btn btn-block btn-primary" data-toggle="modal" data-target="#exampleModalFive">
                                Respond to Ticket
                            </button></a>
                        @endcan

                        @can('isUser')
                        <hr />
                        <a href="#cancel"><button type="button" class="btn btn-block btn-primary" data-toggle="modal" data-target="#exampleModalFive">
                                Cancel Ticket
                            </button></a>
                        @endcan


                    </div><!-- .card-body -->
                </div>

            </div><!-- .col -->

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-9">

                <!-- Options -->
                <div class="card">
                    <div class="card-header">Details</div>
                    <div class="card-body">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Company</th>
                                    <th>Application</th>
                                    <th>Priority</th>
                                    <th>Date Created</th>
                                    <!-- <th>Description</th>
                                    <th>Status</th> -->
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>{{$supportticket->company}}</td>
                                    <td>{{$supportticket->application}}</td>
                                    <td>{{$supportticket->priority}}</td>
                                    <td>{{ \Carbon\Carbon::parse($supportticket->created_at)->format('d/M/Y')}}</td>
                                    <!-- <td>The amount of time to delay between automatically cycling an item. If
                                        false, carousel will not automatically cycle.</td> -->
                                </tr>
                            </tbody>
                        </table>
                        <hr />

                        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-expanded="true">Title</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-expanded="true">Description</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="pills-tabContent">
                            <div class="tab-pane fade show active" id="pills-home" role="tabpanel">
                                {{$supportticket->support_category}}
                            </div>
                            <div class="tab-pane fade" id="pills-profile" role="tabpanel">
                                {{$supportticket->support_description}}
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <hr />

        @can('isUser')
        <h4>Cancel Ticket: {{$supportticket->support_ticket_no}}</h4>
        <hr />
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                <!-- Options -->
                <div class="card">
                    <div class="card-header"></div>
                    <div class="card-body" id="cancel">
                        <form method="post" action="{{action ('SupportTicketController@cancel') }}" enctype='multipart/form-data'>
                            @csrf
                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label for="Support Ticket Number" class="col-form-label">Support Ticket Number</label>
                                    <input type="text" readonly value="{{$supportticket->support_ticket_no}}" class="form-control" id="support_ticket_no" name="support_ticket_no" placeholder="support_ticket_no">
                                </div>
                                <div class="form-group col-md-4">
                                    <!-- <label for="created_by" class="col-form-label">Ticket Created by</label> -->
                                    <input type="text" hidden readonly value="{{Auth::user()->email}}" class="form-control" id="contact_email" name="contact_email" >
                                    <!-- <input type="text" class="form-control" id="created_by" placeholder="Ticket Created by" hidden value="{{Auth::user()->email}}" name="respondent_email"> -->
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label for="inputAddress2" class="col-form-label">Reason for Cancelling: Max Characters: 1000</label>
                                    <textarea class="form-control" name="cancel_reason" rows=11 cols=50 maxlength=1000 required></textarea>
                                </div>
                            </div>

                            <button type="submit" class="btn btn-primary">Submit Ticket</button>
                        </form>

                    </div>
                </div>

            </div>
        </div>
        @endcan
        @can('isCertify')
        <h4>Respond to Ticket: {{$supportticket->support_ticket_no}}</h4>
        <hr />
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                <!-- Options -->
                <div class="card">
                    <div class="card-header"></div>
                    <div class="card-body" id="respond">
                        <form method="post" action="{{action('SupportTicketController@update')}}" enctype='multipart/form-data'>
                            @csrf
                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label for="Support Ticket Number" class="col-form-label">Support Ticket Number </label>
                                    <input type="text" readonly value="{{$supportticket->support_ticket_no}}" class="form-control" id="support_ticket_no" name="support_ticket_no" placeholder="support_ticket_no">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="Support Ticket Number" class="col-form-label">Hours Spent on solution</label>
                                    <input type="number" min="1" class="form-control" name="man_hours" placeholder="Man Hours" required>
                                </div>
                                <div class="form-group col-md-4">
                                    <!-- <label for="created_by" class="col-form-label">Ticket Created by</label> -->
                                    <input type="text" class="form-control" id="created_by" placeholder="Ticket Created by" hidden value="{{Auth::user()->name}}" name="responded_by">
                                    <input type="text" class="form-control" id="created_by" placeholder="Ticket Created by" hidden value="{{Auth::user()->email}}" name="respondent_email">
                                    <input type="text" class="form-control" id="created_by" hidden value="{{$supportticket->contact_email}}" name="contact_email">
                                </div>
                                <!-- <div class="form-group col-md-4">
                                    <label for="contact_email" class="col-form-label">Contact Email</label>
                                    <input type="email" class="form-control" id="contact_email" placeholder="Contact Email" value="{{Auth::user()->email}}" name="contact_email">
                                </div> -->
                            </div>
                            <div class="form-row">


                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label for="inputAddress2" class="col-form-label">Solution Description: Max Characters: 1500</label>
                                    <textarea class="form-control" name="support_description" rows=11 cols=50 maxlength=1500 required></textarea>
                                </div>
                                <div class="form-group">
                                    <input type="text" value="1" readonly class="form-control" hidden id="status" name="status">
                                </div>
                            </div>

                            <button type="submit" class="btn btn-primary">Submit Ticket</button>
                        </form>

                    </div>
                </div>

            </div>
        </div>
        @endcan

        @can('isAdmin')
        <h4>Respond to Ticket: {{$supportticket->support_ticket_no}}</h4>
        <hr />
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                <!-- Options -->
                <div class="card">
                    <div class="card-header"></div>
                    <div class="card-body" id="respond">
                        <form method="post" action="{{action('SupportTicketController@update')}}" enctype='multipart/form-data'>
                            @csrf
                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label for="Support Ticket Number" class="col-form-label">Support Ticket Number </label>
                                    <input type="text" readonly value="{{$supportticket->support_ticket_no}}" class="form-control" id="support_ticket_no" name="support_ticket_no" placeholder="support_ticket_no">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="Support Ticket Number" class="col-form-label">Hours Spent on solution</label>
                                    <input type="number" min="1" class="form-control" name="man_hours" placeholder="Man Hours" required>
                                </div>
                                <div class="form-group col-md-4">
                                    <!-- <label for="created_by" class="col-form-label">Ticket Created by</label> -->
                                    <input type="text" class="form-control" id="created_by" placeholder="Ticket Created by" hidden value="{{Auth::user()->name}}" name="responded_by">
                                    <input type="text" class="form-control" id="created_by" placeholder="Ticket Created by" hidden value="{{Auth::user()->email}}" name="respondent_email">
                                    <input type="text" class="form-control" id="created_by" hidden value="{{$supportticket->contact_email}}" name="contact_email">
                                </div>
                                <!-- <div class="form-group col-md-4">
                                    <label for="contact_email" class="col-form-label">Contact Email</label>
                                    <input type="email" class="form-control" id="contact_email" placeholder="Contact Email" value="{{Auth::user()->email}}" name="contact_email">
                                </div> -->
                            </div>
                            <div class="form-row">


                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label for="inputAddress2" class="col-form-label">Solution Description: Max Characters: 1500</label>
                                    <textarea class="form-control" name="support_description" rows=11 cols=50 maxlength=1500 required></textarea>
                                </div>
                                <div class="form-group">
                                    <input type="text" value="1" readonly class="form-control" hidden id="status" name="status">
                                </div>
                            </div>

                            <button type="submit" class="btn btn-primary">Submit Ticket</button>
                        </form>

                    </div>
                </div>

            </div>
        </div>
        @endcan

    </div>
</main>
@include('sweetalert::alert')
@stop