@extends('layouts.master')
@section('content')
<main id="main-container">
    <div class="content">
        <h2 class="content-heading">Open Tickets </h2>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">Open tickets</div>
                    <div class="card-body">

                        <table id="example1" class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Ticket Number</th>
                                    <th>Company</th>
                                    <th>Application</th>
                                    <th>Ticket Created by</th>
                                    <th>Creation Date</th>
                                    <th>Contact Email</th>
                                    <!-- <th>Support Title</th>
                                    <th>Support Description</th> -->

                                </tr>
                            </thead>
                            @can('isAdmin')
                            <tbody>
                                @forelse($open_tickets as $mytickets)
                                <tr>
                                <td><a href="{{ route('opentickets.ticket-details',['id'=>$mytickets->id])}}">{{ $mytickets->support_ticket_no }}</a></td>
                                    <!-- <td>{{ $mytickets->support_ticket_no }}</td> -->
                                    <td>{{ $mytickets->company }}</td>
                                    <td>{{ $mytickets->application }}</td>
                                    <td>{{ $mytickets->created_by}}</td>
                                    <td>{{ \Carbon\Carbon::parse($mytickets->created_at)->format('d/M/Y')}}</td>
                                    <td>{{ $mytickets->contact_email }}</td>
                                    <!-- <td>{{ $mytickets->support_category }}</td>
                                    <td>{{ $mytickets->support_description }}</td> -->
                                </tr>
                                @empty
                                <tr>
                                    <p>No Data availabe</p>
                                </tr>
                                @endforelse
                            </tbody>
                            @endcan
                            @can('isCertify')
                            <tbody>
                                @forelse($open_tickets as $mytickets)
                                <tr>
                                <td><a href="{{ route('opentickets.ticket-details',['id'=>$mytickets->id])}}">{{ $mytickets->support_ticket_no }}</a></td>
                                    <!-- <td>{{ $mytickets->support_ticket_no }}</td> -->
                                    <td>{{ $mytickets->company }}</td>
                                    <td>{{ $mytickets->application }}</td>
                                    <td>{{ $mytickets->created_by}}</td>
                                    <td>{{ \Carbon\Carbon::parse($mytickets->created_at)->format('d/M/Y')}}</td>
                                    <td>{{ $mytickets->contact_email }}</td>
                                    <!-- <td>{{ $mytickets->support_category }}</td>
                                    <td>{{ $mytickets->support_description }}</td> -->
                                </tr>
                                @empty
                                <tr>
                                    <p>No Data availabe</p>
                                </tr>
                                @endforelse
                            </tbody>
                            @endcan
                            @can('isUser')
                            <tbody>
                                @forelse($open_tickets_mine as $mytickets)
                                <tr>
                                    <!-- <td>{{ $mytickets->support_ticket_no }}</td> -->
                                    <td><a href="{{ route('opentickets.ticket-details',['id'=>$mytickets->id])}}">{{ $mytickets->support_ticket_no }}</a></td>
                                    <td>{{ $mytickets->company }}</td>
                                    <td>{{ $mytickets->application }}</td>
                                    <td>{{ $mytickets->created_by}}</td>
                                    <td>{{ \Carbon\Carbon::parse($mytickets->created_at)->format('d/M/Y')}}</td>
                                    <td>{{ $mytickets->contact_email }}</td>
                                    <!-- <td>{{ $mytickets->support_category }}</td>
                                    <td>{{ $mytickets->support_description }}</td> -->
                                </tr>
                                @empty
                                <tr>
                                    <p>No Data availabe</p>
                                </tr>
                                @endforelse
                            </tbody>
                            @endcan
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@include('sweetalert::alert')
@stop