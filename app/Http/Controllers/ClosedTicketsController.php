<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SupportTicket;
use App\SupportSolution;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\DB;

class ClosedTicketsController extends Controller
{
    //
    public function __construct(){
        $this->middleware(['auth'],['verified']);
    }
    public function index(){
        $user_name = Auth::user()->name;
        if(session('success_message')){
            Alert::success('Hi '.$user_name, session('success_message'));
        }
        $current_email = Auth::user()->email;        
        $closed_tickets = SupportTicket::where('status', '2')->get();
        $closed_tickets_mine = SupportTicket::where('status', '2')->where('contact_email', $current_email)->get();
        return view ('closedtickets.index',compact('closed_tickets','closed_tickets_mine'));

    }
    public function show(Request $request, $id){
        $current_email = Auth::user()->email;
        $supportticket = SupportTicket::find($id);
        $ticket_id = $supportticket->support_ticket_no;
        $ticket_num  = $request->get('ticket_num');
        // $request->get('support_ticket_no');
        // $supportticket_no = SupportTicket::select('support_ticket_no')->where('id', $id)->first();   
        // $supportticket_no = DB::select("select support_ticket_no from support_tickets where id = $ticket_id");     
        // $ticket_no = SupportSolution::find($id)->where('id', $supportticket);   
        $ticket_response = DB::select("select * from support_solutions where support_ticket_no=$ticket_id ");
        // $ticket_response = DB::select('select * from support_solutions where active = ?', [1]);
        $closed_tickets = SupportSolution::all();  
        // dd($ticket_response);      
        return view('closedtickets.ticket-details',compact('supportticket','ticket_response','closed_tickets'));
    }
}
