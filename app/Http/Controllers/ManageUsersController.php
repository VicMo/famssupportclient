<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use RealRashid\SweetAlert\Facades\Alert;
use App\Mail\SupportTeamMail;
use App\Mail\SupportTicketMail;
use Illuminate\Support\Facades\Mail;

class ManageUsersController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware([
            'auth',
            // 'verified'
        ]);
    }
    public function index()
    {
        if (session('success_message')) {
            Alert::success('Hi ', session('success_message'));
        }
        if (session('error_message')) {
            Alert::error('Hi ', session('error_message'));
        }
        // $view_users = User::all();
        $view_users = User::where('user_type', 'certify')->orwhere('user_type', 'admin')->get();
        return view('manage-users.index', compact('view_users'));
    }
    public function newuser()
    {
        return view('manage-users.new-user');
    }

    public function store(Request $request)
    {
        if (session('error_message')) {
            Alert::error('Hi ', session('error_message'));
        }
        // Post in users Table.
        //We check if the email exists

        try {
            $new_user = new User();
            $new_user->name = $request->name;
            $new_user->email = $request->email;
            $new_user->password = Hash::make($request->password);
            $new_user->user_type = $request->user_type;
            $new_user->save();

            $pass = $new_user->password = $request->password;

            try {
                $myEmail = $new_user->email;
                $password = $pass;
                $details = [
                    // 'subject' => 'Ticket Number: '.$support_ticket->support_ticket_no,
                    'title' => 'New account Details',
                    'url' => 'http://dev.tamarix.co.ke/login',
                    'message' => 'We have created an account for you. Use these credentials to access your account: Email: ' . $myEmail . ' and the password is: ' . $password,
                    'description' => ''
                ];
                Mail::to($myEmail)->send(new SupportTeamMail($details));
            } catch (\Exception $e) {
                return redirect('manage-users')->withErrorMessage('Error Sending email but record submited successfully.');
            }
        } catch (\Exception $e) {
            return redirect('manage-users')->withErrorMessage('Either the email service is down or the user already exists.');
        }

        return redirect('manage-users')->withSuccessMessage('User created Successfully.');
    }
}
