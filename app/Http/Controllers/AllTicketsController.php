<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SupportTicket;
use Illuminate\Support\Facades\Auth;
use App\CLient;


class AllTicketsController extends Controller
{
    //
    public function __construct(){
        $this->middleware(['auth'],['verified']);
    }
    public function index(){
        $current_email = Auth::user()->email;
        //pull all support tickets
        $alltickets = SupportTicket::all();
        //we get the current user email.         
        $alltickets_mine = SupportTicket::where('contact_email', $current_email)->get();

        return view('alltickets.index', compact('alltickets','alltickets_mine'));
    }
    public function show($id){
        $supportticket = SupportTicket::find($id);
        $tickets = SupportTicket::all();
        return view('alltickets.ticket-details',compact('supportticket','tickets'));
    }
}
