<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SupportTicket;
use Illuminate\Support\Facades\Auth;

class CancelledTicketsController extends Controller
{
    //
    public function __construct(){
        $this->middleware(['auth'],['verified']);
    }
    public function index(){
        $current_email = Auth::user()->email;
        
        $cancelled_tickets = SupportTicket::where('status', '3')->get();
        $cancelled_tickets_mine = SupportTicket::where('status', '3')->where('contact_email', $current_email)->get();
        return view ('cancelledtickets.index',compact('cancelled_tickets','cancelled_tickets_mine'));        
    }
    public function show($id){
        $current_email = Auth::user()->email;
        $supportticket = SupportTicket::find($id);
        $tickets = SupportTicket::all();
        return view('cancelledtickets.ticket-details',compact('supportticket','tickets'));
    }
    
}
