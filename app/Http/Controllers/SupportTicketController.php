<?php

namespace App\Http\Controllers;

use App\SupportTicket;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\Client;
use App\User;
use Illuminate\Support\Facades\Hash;

use Illuminate\Support\Facades\Auth;

use RealRashid\SweetAlert\Facades\Alert;
use App\SupportSolution;
use Illuminate\Support\Facades\DB;
use App\Mail\SupportTeamMail;
use App\Mail\SupportTicketMail;
use Illuminate\Support\Facades\Mail;
use App\CancelledTicket;
use App\Project;


class SupportTicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware(['auth'], ['verified']);
    }
    public function index()
    {
        //
        $user_name = Auth::user()->name;
        if (session('success_message')) {
            Alert::success('Hi ' . $user_name, session('success_message'));
        }
        if (session('error_message')) {
            Alert::error('Hello ' . $user_name, session('error_message'));
        }
        $projects = Project::all();
        return view('create-ticket.index', compact('projects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try {
            $request->validate([
                'support_ticket_no' => 'required',
                'contact_email' => 'required',
                'company' => 'required',
                'application' => 'required',
                'priority' => 'required',
                'status' => 'required|integer',
                'attachement' => 'max:5120'
            ]);
            $cover = $request->file('attachement');
            if (!empty($cover)) {
                $extension = $cover->getClientOriginalExtension();
                Storage::disk('public')->put($cover->getFilename() . '.' . $extension,  File::get($cover));
                $support_ticket = new SupportTicket();
                $support_ticket->support_ticket_no = $request->support_ticket_no;
                $support_ticket->contact_email = $request->contact_email;
                $support_ticket->company = $request->company;
                $support_ticket->application = $request->application;
                $support_ticket->priority = $request->priority;
                $support_ticket->support_category = $request->support_category;
                $support_ticket->support_description = $request->support_description;
                $support_ticket->created_by = $request->created_by;
                $support_ticket->status = $request->status;
                $support_ticket->mime = $cover->getClientMimeType();
                $support_ticket->original_filename = $cover->getClientOriginalName();
                $support_ticket->filename = $cover->getFilename() . '.' . $extension;
            } else {
                $support_ticket = new SupportTicket();
                $support_ticket->support_ticket_no = $request->support_ticket_no;
                $support_ticket->contact_email = $request->contact_email;
                $support_ticket->company = $request->company;
                $support_ticket->application = $request->application;
                $support_ticket->priority = $request->priority;
                $support_ticket->support_category = $request->support_category;
                $support_ticket->support_description = $request->support_description;
                $support_ticket->created_by = $request->created_by;
                $support_ticket->status = $request->status;
            }
            $support_ticket->save();
        } catch (\Exception $e) {
            return redirect('create-ticket')->withErrorMessage('Kindly make sure you have inputs for the required fields and try again');
        }
        try {
            $myEmail = $support_ticket->contact_email;
            $details = [
                // 'subject' => 'Ticket Number: '.$support_ticket->support_ticket_no,
                'title' => 'Support request for: ' . $support_ticket->support_ticket_no,
                'url' => 'http://dev.tamarix.co.ke/login',
                'message' => 'We have received your support request and we are working on it. 
            Kindly track your ticket through your account. '
            ];
            Mail::to($myEmail)->send(new SupportTicketMail($details));

            //Send to staff
            $created_by = $support_ticket->created_by;
            $user_email = $support_ticket->contact_email;
            $request = $support_ticket->support_description;
            $myEmails = ['humphreybrian43@gmail.com', 'support@tamarix.co.ke'];
            $details = [
                'subject' => 'Ticket Number: ' . $support_ticket->support_ticket_no,
                'title' => 'Support request for: ' . $support_ticket->support_ticket_no,
                'url' => 'http://dev.tamarix.co.ke/login',
                'message' => 'Kindly respond to this support request created by ' . $created_by . ' with email ' . $user_email . '. The request is: ',
                'description' => $request
            ];
            Mail::to($myEmails)->send(new SupportTeamMail($details));
        } catch (\Exception $e) {
            return redirect('create-ticket')->withSuccessMessage('Your Support request has been submited successfully. Email service is offlineZ');
        }

        return redirect('create-ticket')->withSuccessMessage('Your Support request has been submited successfully.');
        // return $support_ticket;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SupportTicket  $supportTicket
     * @return \Illuminate\Http\Response
     */
    public function show(SupportTicket $supportTicket)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SupportTicket  $supportTicket
     * @return \Illuminate\Http\Response
     */
    public function edit(SupportTicket $supportTicket)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SupportTicket  $supportTicket
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $user_name = Auth::user()->name;
        if (session('success_message')) {
            Alert::success('Hi ' . $user_name, session('success_message'));
        }
        // new SupportTicket()
        $ticket_solution = new SupportSolution();
        $ticket_solution->support_ticket_no = $request->get('support_ticket_no');
        $ticket_solution->responded_by = $request->get('responded_by');
        $ticket_solution->respondent_email = $request->get('respondent_email');
        $ticket_solution->man_hours = $request->get('man_hours');
        $ticket_solution->support_description = $request->get('support_description');
        $ticket_solution->status = $request->get('status');
        $ticket_solution->save();

        $ticket_num = $ticket_solution->support_ticket_no = $request->get('support_ticket_no');
        $contact_email = $ticket_solution->contact_email = $request->get('contact_email');

        DB::table('support_tickets')
            ->where('support_ticket_no', $ticket_num)
            ->update(['status' => 2]);

        // $owner_email = SupportTicket::select('contact_email')->where('support_ticket_no', $ticket_num)->get();
        $owner_email = DB::table('support_tickets')
            ->where('support_ticket_no', '=', $ticket_num)
            ->pluck('contact_email');

        try {
            $myEmail = $contact_email;
            $details = [
                // 'subject' => 'Ticket Number: '.$support_ticket->support_ticket_no,
                'title' => 'Support request for: ' . $ticket_num,
                'url' => 'http://dev.tamarix.co.ke/login',
                'message' => 'We have Responded to the request. '
            ];
            Mail::to($myEmail)->send(new SupportTicketMail($details));
        } catch (\Exception $e) {
            return redirect('opentickets')->withSuccessMessage('Thank you for responding to the ticket. Email service is down email was not sent');
        }



        return redirect('opentickets')->withSuccessMessage('Thank you for responding to the ticket.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SupportTicket  $supportTicket
     * @return \Illuminate\Http\Response
     */
    public function destroy(SupportTicket $supportTicket)
    {
        //
    }

    public function cancel(Request $request)
    {
        $user_name = Auth::user()->name;
        if (session('success_message')) {
            Alert::success('Hi ' . $user_name, session('success_message'));
        }
        // new SupportTicket()

        $ticket_id = $request->get('id');
        $ticket_status = $request->get('status');
        $contact_email = $request->get('contact_email');
        $support_ticket_no = $request->get('support_ticket_no');
        $cancel_reason = $request->get('cancel_reason');




        // dd($contact_email,$ticket_id,$ticket_status);

        // new SupportTicket()
        $cancelled_tickets = new CancelledTicket();
        $cancelled_tickets->contact_email = $contact_email;
        $cancelled_tickets->support_ticket_no = $support_ticket_no;
        $cancelled_tickets->cancel_reason = $cancel_reason;
        $cancelled_tickets->save();

        // DB::table('support_tickets')
        //     ->where('id', $ticket_id)
        //     ->update(['status' => 3]);

        DB::table('support_tickets')
            ->where('support_ticket_no', $support_ticket_no)
            ->update(['status' => 3]);


        try {
            $myEmail = $contact_email;
            $details = [
                // 'subject' => 'Ticket Number: '.$support_ticket->support_ticket_no,
                'title' => 'Support request for: ' . $support_ticket_no . $ticket_id,
                'url' => 'http://dev.tamarix.co.ke/login',
                'message' => 'You have cancelled ticket. '
            ];
            Mail::to($myEmail)->send(new SupportTicketMail($details));
        } catch (\Exception $e) {
            return redirect('opentickets')->withSuccessMessage('You have successfully cancelled the ticket. But the email notification is down');
        }



        return redirect('opentickets')->withSuccessMessage('You have successfully cancelled the ticket.');
    }
}
