<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SupportTicket;
use App\Client;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $current_email = Auth::user()->email; 
        //COunt tickets for specific user
        $all_tickets_mine = SupportTicket::where('contact_email', $current_email)->count();
        $open_tickets_mine = SupportTicket::where('status', '1')->where('contact_email', $current_email)->count();
        $closed_tickets_mine = SupportTicket::where('status', '2')->where('contact_email', $current_email)->count();
        $cancelled_tickets_mine = SupportTicket::where('status', '3')->where('contact_email', $current_email)->count();
        //COunt tickets 
        $all_tickets = SupportTicket::all()->count();
        $open_tickets = SupportTicket::where('status', '1')->get()->count();
        $closed_tickets = SupportTicket::where('status', '2')->get()->count();
        $cancelled_tickets = SupportTicket::where('status', '3')->get()->count();
        //Pull Tickets from
        $alltickets = SupportTicket::all();
        $alltickets_mine = SupportTicket::where('contact_email', $current_email)->get();

        return view('home', compact('alltickets','all_tickets','open_tickets','closed_tickets','cancelled_tickets','all_tickets_mine','alltickets_mine','open_tickets_mine','closed_tickets_mine','cancelled_tickets_mine'));
    }
}
